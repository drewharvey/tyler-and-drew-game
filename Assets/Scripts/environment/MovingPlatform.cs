﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;
    public float xMoveDist;
    public float yMoveDist;

    private float xDirection;
    private float yDirection;
    private Vector2 startingPos;
    private Vector2 frontBoundry;
    private Vector2 backBoundry;

	// Use this for initialization
	void Start () {
        xDirection = 1f;
        yDirection = 1f;
        startingPos = this.transform.position;
        frontBoundry = new Vector2(startingPos.x + xMoveDist, startingPos.y + yMoveDist);
        backBoundry = new Vector2(startingPos.x - xMoveDist, startingPos.y - yMoveDist);
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x > frontBoundry.x) {
            xDirection = -1f;
        }
        if (transform.position.x < backBoundry.x) {
            xDirection = 1f;
        }
        if (transform.position.y > frontBoundry.y) {
            yDirection = -1f;
        }
        if (transform.position.y < backBoundry.y) {
            yDirection = 1f;
        }

        transform.position = new Vector2(
            transform.position.x + xSpeed * xDirection * Time.deltaTime, 
            transform.position.y + ySpeed * yDirection * Time.deltaTime);
	}
}
