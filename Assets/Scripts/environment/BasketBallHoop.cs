﻿using UnityEngine;
using System.Collections;

public class BasketBallHoop : MonoBehaviour {

    public Color hitColor;

    private Color idleColor;
    private SpriteRenderer sprite;

    // Use this for initialization
    void Start() {
        sprite = GetComponent<SpriteRenderer>();
        idleColor = sprite.color;
    }

    // Update is called once per frame
    void Update() {

    }

    private void flashHoop(Color color) {
        sprite.color = color;
        StartCoroutine(Timer.Start(1, () => {
            sprite.color = idleColor;
        }));
    }

    #region Collision
    protected void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.transform.tag != "Bullet") {
            return;
        }
        if (other.gameObject.rigidbody2D != null
            && other.gameObject.rigidbody2D.velocity.y > 0) {
            flashHoop(hitColor);
        } else {

        }
    }
    protected void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.transform.tag != "Bullet") {
            return;
        }
        if (other.gameObject.rigidbody2D != null
            && other.gameObject.rigidbody2D.velocity.y > 0) {
            flashHoop(hitColor);
        } else {

        }
    }
    #endregion

}