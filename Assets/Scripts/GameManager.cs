﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static GameObject[] getPlayerGameObjects() {
        return GameObject.FindGameObjectsWithTag("Player");
    }

    public static GameObject getPlayerGameObject(int controllerNumber) {
        GameObject player = null;
        foreach (Player p in getPlayerScripts()) {
            if (p.controllerNumber == controllerNumber) {
                player = p.gameObject;
            }
        }
        return player;
    }

    public static Player[] getPlayerScripts() {
        return GameObject.FindObjectsOfType<Player>();
    }

    public static Player getPlayerScript(int controllerNumber) {
        Player player = null;
        foreach (Player p in getPlayerScripts()) {
            if (p.controllerNumber == controllerNumber) {
                player = p;
            }
        }
        return player;
    }
}
