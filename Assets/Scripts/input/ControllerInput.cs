﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerInput : MonoBehaviour {

    public ControllerNumber controllerNumber;

	private Dictionary<string, string>[] buttonTable;

	// Use this for initialization
	void Start () {
		initButtonMap();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector2 GetDirection() {
		Vector2 direction = new Vector2();
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
		return direction;
	}

	public bool isButtonDown(string button) {
		return Input.GetButtonDown(getButton(button));
	}

	private void initButtonMap() {
		buttonTable = new Dictionary<string, string>[4];
        // player 1
        buttonTable[0] = new Dictionary<string, string>();
        buttonTable[0].Add("moveHorizontalAxis", "Horizontal");
        buttonTable[0].Add("moveVerticalAxis", "Vertical");
        buttonTable[0].Add("aimHorizontalAxis", "Horizontal");
        buttonTable[0].Add("aimHorizontalAxis", "Horizontal");
		buttonTable[0].Add("jump", "Jump");
		buttonTable[0].Add("attack", "Fire1");
		buttonTable[0].Add("skill1", "Fire2");
		buttonTable[0].Add("skill2", "Fire3");
        // player 2
        buttonTable[1] = new Dictionary<string, string>();
        buttonTable[1].Add("jump", "Jump");
        buttonTable[1].Add("attack", "Fire1");
        buttonTable[1].Add("skill1", "Fire2");
        buttonTable[1].Add("skill2", "Fire3");
        // player 3
        buttonTable[2] = new Dictionary<string, string>();
        buttonTable[2].Add("jump", "Jump");
        buttonTable[2].Add("attack", "Fire1");
        buttonTable[2].Add("skill1", "Fire2");
        buttonTable[2].Add("skill2", "Fire3");
        // player 4
        buttonTable[3] = new Dictionary<string, string>();
        buttonTable[3].Add("jump", "Jump");
        buttonTable[3].Add("attack", "Fire1");
        buttonTable[3].Add("skill1", "Fire2");
        buttonTable[3].Add("skill2", "Fire3");
	}

	private string getButton(string button) {
		return buttonTable[(int)controllerNumber][button.ToLower()];
	}

    public enum ControllerNumber {
        ONE = 0,
        TWO = 1,
        THREE = 2,
        FOUR = 3
    };

}
