﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

static class KeyBindings {

    private static Dictionary<string, KeyCode>[] keyCodeTable;
    private static Dictionary<string, string>[] axisNameTable;

    static KeyBindings() {
        initMap();
    }

    public static KeyCode getKeyCode(int controllerNumber, string action) {
        KeyCode code = keyCodeTable[controllerNumber][action];
        return code;
    }

    public static string getAxisName(int controllerNumber, string action) {
        string name = axisNameTable[controllerNumber][action];
        return name;
    }

    private static void initMap() {
        // KeyCode's are direct input
        keyCodeTable = new Dictionary<string, KeyCode>[4];
        // string names such as Horizontal-1 are mapped inside Unity > Input
        axisNameTable = new Dictionary<string, string>[4];
        // player 1
        axisNameTable[0] = new Dictionary<string, string>();
        axisNameTable[0].Add("moveHorizontalAxis", "Horizontal-1");
        axisNameTable[0].Add("moveVerticalAxis", "Vertical-1");
        axisNameTable[0].Add("aimHorizontalAxis", "rightHorizontal-1");
        axisNameTable[0].Add("aimVerticalAxis", "rightVertical-1");
        keyCodeTable[0] = new Dictionary<string, KeyCode>();
        keyCodeTable[0].Add("jump", KeyCode.Joystick1Button4);
        keyCodeTable[0].Add("attack", KeyCode.Joystick1Button5);
        keyCodeTable[0].Add("reload", KeyCode.Joystick1Button2);
        keyCodeTable[0].Add("dash", KeyCode.Joystick1Button1);
        // player 2
        axisNameTable[1] = new Dictionary<string, string>();
        axisNameTable[1].Add("moveHorizontalAxis", "Horizontal-2");
        axisNameTable[1].Add("moveVerticalAxis", "Vertical-2");
        axisNameTable[1].Add("aimHorizontalAxis", "rightHorizontal-2");
        axisNameTable[1].Add("aimVerticalAxis", "rightVertical-2");
        keyCodeTable[1] = new Dictionary<string, KeyCode>();
        keyCodeTable[1].Add("jump", KeyCode.Joystick2Button4);
        keyCodeTable[1].Add("attack", KeyCode.Joystick2Button5);
        keyCodeTable[1].Add("reload", KeyCode.Joystick2Button2);
        keyCodeTable[1].Add("dash", KeyCode.Joystick2Button1);
        // player 3
        axisNameTable[2] = new Dictionary<string, string>();
        axisNameTable[2].Add("moveHorizontalAxis", "Horizontal-3");
        axisNameTable[2].Add("moveVerticalAxis", "Vertical-3");
        axisNameTable[2].Add("aimHorizontalAxis", "rightHorizontal-3");
        axisNameTable[2].Add("aimVerticalAxis", "rightVertical-3");
        keyCodeTable[2] = new Dictionary<string, KeyCode>();
        keyCodeTable[2].Add("jump", KeyCode.Joystick3Button4);
        keyCodeTable[2].Add("attack", KeyCode.Joystick3Button5);
        keyCodeTable[2].Add("reload", KeyCode.Joystick3Button2);
        keyCodeTable[2].Add("dash", KeyCode.Joystick3Button1);
        // player 4
        axisNameTable[3] = new Dictionary<string, string>();
        axisNameTable[3].Add("moveHorizontalAxis", "Horizontal-4");
        axisNameTable[3].Add("moveVerticalAxis", "Vertical-4");
        axisNameTable[3].Add("aimHorizontalAxis", "rightHorizontal-4");
        axisNameTable[3].Add("aimVerticalAxis", "rightVertical-4");
        keyCodeTable[3] = new Dictionary<string, KeyCode>();
        keyCodeTable[3].Add("jump", KeyCode.Joystick4Button4);
        keyCodeTable[3].Add("attack", KeyCode.Joystick4Button5);
        keyCodeTable[3].Add("reload", KeyCode.Joystick4Button2);
        keyCodeTable[3].Add("dash", KeyCode.Joystick4Button1);
    }

}
