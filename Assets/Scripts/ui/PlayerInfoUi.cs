﻿using UnityEngine;
using System.Collections;

public class PlayerInfoUi : MonoBehaviour {

    public int controllerNumber;

    Player player;
    IWeapon weapon;

	// Use this for initialization
	void Start () {
        player = GameManager.getPlayerScript(controllerNumber);
        weapon = player.getWeaponScript();
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null || weapon == null) {
            Start();
        }
        string text = "";
        // HP
        text += "HP: " + player.getHP() + "/" + player.getMaxHP() + "\n";
        // Clip size or reload
        if (weapon.getReloadTimer() > 0) {
            text += "Reloading: " + weapon.getReloadTimer() + "\n";
        } else {
            text += "Ammo: " + weapon.getCurrentClip() + "/" + weapon.getClipSize() + "\n";
        }
        // Shot cooldown
        text += "Fire CD: " + weapon.getFireTimer() + "\n";
        // weapon name
        text += weapon.getWeaponName() + "\n";
        this.guiText.text = text;
	}
}
