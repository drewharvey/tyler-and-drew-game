﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public int controllerNumber;
    public int baseHP;
    public float baseSpeed;
    public float baseJumpSpeed;
    public float gravity;
    public float dashSpeedMultiplyer;
    public float dashDuration;
    public GameObject weapon;

    private int healthPoints;

    private float speedMultiplyer = 1f;
    private float maxSpeed = 100f;
    private Vector2 moveDirection;
    private Vector2 prevMoveDirection;
    private Vector2 aimDirection;
    private Vector2 prevAimDirection;

    private bool canJump = true;
    private bool canFire = true;
    private bool canDash = true;

    private bool isDashing = false;
    private bool isKnockedBack = false;

	// Use this for initialization
	void Start () {
        healthPoints = baseHP;
        rigidbody2D.gravityScale = gravity;
        moveDirection = Vector2.zero;
        prevMoveDirection = Vector2.zero;
        aimDirection = Vector2.zero;
        prevAimDirection = Vector2.zero;
        weapon = Instantiate(weapon) as GameObject;
	}
	
	// Update is called once per frame
	void Update () {

        // remember last update's position
        prevMoveDirection.Set(moveDirection.x, moveDirection.y);
        prevAimDirection.Set(aimDirection.x, aimDirection.y);
        // get player input
        moveDirection.Set(
            Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "moveHorizontalAxis")),
            Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "moveVerticalAxis")));
        aimDirection.Set(
            Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "aimHorizontalAxis")),
            -Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "aimVerticalAxis"))); // y-axis is negated
        aimDirection.Normalize();

        float xVelocity = 0f;
        float yVelocity = 0f;

        if (isDashing) {
            moveDirection.Set(prevMoveDirection.x, prevMoveDirection.y);
            xVelocity = moveDirection.x * baseSpeed * speedMultiplyer;
            yVelocity = moveDirection.y * baseSpeed * speedMultiplyer;
        } else if (isKnockedBack) {
            xVelocity = rigidbody2D.velocity.x + (moveDirection.x * speedMultiplyer);
            yVelocity = rigidbody2D.velocity.y;
        } else {
            // move normally
            xVelocity = moveDirection.x * baseSpeed * speedMultiplyer;
            yVelocity = rigidbody2D.velocity.y;
        }
        // cap speeds
        if (xVelocity > maxSpeed) {
            xVelocity = maxSpeed;
        }
        if (yVelocity > maxSpeed) {
            yVelocity = maxSpeed;
        }
        // finalize position
        rigidbody2D.velocity = new Vector2(xVelocity, yVelocity);

        // jump button
        if (Input.GetKeyDown(KeyBindings.getKeyCode(controllerNumber, "jump"))) {
            jump();
        }
        // attack button
        if (Input.GetKeyDown(KeyBindings.getKeyCode(controllerNumber, "attack"))) {
            fire();
        }
        // reload button
        if (Input.GetKeyDown(KeyBindings.getKeyCode(controllerNumber, "reload"))) {
            reload();
        }
        // dash button
        if (Input.GetKeyDown(KeyBindings.getKeyCode(controllerNumber, "dash"))) {
            dash();
        }

        // make weapon stick to us
        weapon.transform.position = transform.position;

        // simulate gun movement
        float aimLength = 5f;
        Vector3 endPoint = new Vector3(aimDirection.x, aimDirection.y, transform.position.z);
        endPoint *= aimLength;
        endPoint += transform.position;
        Debug.DrawLine(transform.position, endPoint, Color.white);
	}

    public int getHP() {
        return healthPoints;
    }

    public int getMaxHP() {
        return baseHP;
    }

    public IWeapon getWeaponScript() {
        IWeapon w = weapon.GetComponent(typeof(IWeapon)) as IWeapon;
        return w;
    }

    private void jump() {
        if (canJump) {
            canJump = false;
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, baseJumpSpeed);
        }
    }

    private void fire() {
        if (!canFire) {
            return;
        }
        IWeapon w = getWeaponScript();
        if (w.getFireTimer() > 0) {
            return;
        }
        float direction = 1f;
        if (aimDirection.x < 0) {
            direction = -1f;
        }
        Vector3 position = new Vector3(rigidbody2D.position.x + (3 * direction), rigidbody2D.position.y, transform.position.z);
        w.fire(position, aimDirection);
    }

    private void reload() {
        IWeapon w = getWeaponScript();
        w.reload();
    }

    private void dash() {
        if (canDash) {
            canDash = false;
            isDashing = true;
            speedMultiplyer = dashSpeedMultiplyer;
            moveDirection.Set(
                Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "moveHorizontalAxis")),
                Input.GetAxis(KeyBindings.getAxisName(controllerNumber, "moveVerticalAxis")));
            moveDirection.Normalize();
            StartCoroutine(Timer.Start(dashDuration, () => {
                canDash = true;
                isDashing = false;
                speedMultiplyer = 1f;
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0f);
            }));
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.tag == "Solid") {
            // don't enable jump if we hit our head
            if (rigidbody2D.velocity.y <= 0) {
                canJump = true;
            }
        }

        if (other.transform.tag == "Bullet") {
            handleBulletCollision(other.gameObject);
        } else if (other.transform.tag == "Explosion"
            || other.transform.tag == "DoesDamage") {
            takeDamage(other.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag == "Bullet") {
            handleBulletCollision(other.gameObject);
        } else if (other.transform.tag == "Explosion"
            || other.transform.tag == "DoesDamage") {
            takeDamage(other.gameObject);
        }
    }

    private void handleBulletCollision(GameObject other) {
        // dashing into a bullet avoids damage
        if (isDashing) {
            Debug.Log("DODGE");
        } else {
            takeDamage(other.gameObject);
            IBullet bullet = other.gameObject.GetComponent(typeof(IBullet)) as IBullet;
            bullet.onPlayerHit();
        }
    }

    private void takeDamage(GameObject other) {
        DoesDamage damage = other.GetComponent(typeof(DoesDamage)) as DoesDamage;
        if (damage != null) {
            healthPoints -= damage.getDamage();
            knockBack(other);
            Debug.Log("Damage = " + damage.getDamage());
        }
    }

    private void knockBack(GameObject other) {
        isKnockedBack = true;
        float force = 50f;
        // we want to send the player in the opposite direction
        Vector2 pushDirection = transform.position - other.transform.position;
        pushDirection.Normalize();
        pushDirection *= force;
        rigidbody2D.velocity = pushDirection;
        StartCoroutine(Timer.Start(0.5f, () => {
            isKnockedBack = false;
        }));
    }
}
