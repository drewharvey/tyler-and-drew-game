﻿using UnityEngine;
using System.Collections;

interface DoesDamage {
    int getDamage();
    void setDamage(int dmg);
}
