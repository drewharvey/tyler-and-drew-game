﻿using UnityEngine;
using System.Collections;

public class Longbow: BaseWeapon {
    override public void Start() {
        base.Start();
        this.name = "Longbow";
    }

    public override string getWeaponName() {
        return "Longbow";
    }
}
