﻿using UnityEngine;
using System.Collections;

public class GrenadeLauncher : BaseWeapon {
    virtual public void Start() {
        base.Start();
        this.name = "Grenade Launcher";
        Debug.Log("Name: " + this.getWeaponName());
    }
}
