﻿using UnityEngine;
using System.Collections;

public class BaseWeapon : MonoBehaviour, IWeapon {

    public GameObject bulletPrefab;
    public int clipSize;
    public float bulletSpeed;
    public float shotCooldown;
    public float reloadDuration;

    protected string name;
    protected float fireTimer;
    protected float altFireTimer;
    protected float reloadTimer;
    protected int bulletsInClip;


    // Use this for initialization
    virtual public void Start() {
        Debug.Log("Initializing BaseWeapon");
        fireTimer = 0;
        altFireTimer = 0;
        reloadTimer = 0;
        bulletsInClip = clipSize;
    }

    // Update is called once per frame
    virtual public void Update() {
        if (fireTimer > 0) {
            fireTimer -= Time.deltaTime;
            if (fireTimer < 0) {
                fireTimer = 0;
            }
        }
        if (reloadTimer > 0) {
            reloadTimer -= Time.deltaTime;
            if (reloadTimer <= 0) {
                completeReload();
            }
        }
        // auto reload
        if (bulletsInClip < 1 && reloadTimer <= 0) {
            reload();
        }
    }

    virtual public string getWeaponName() {
        return name;
    }

    virtual public void fire(Vector3 startPosition, Vector2 direction) {
        if (bulletsInClip < 1) {
            return;
        }
        // cancel reload
        cancelReload();
        fireTimer = shotCooldown;
        bulletsInClip--;
        // create a new bullet
        GameObject bullet = (Instantiate(bulletPrefab, startPosition, transform.rotation)) as GameObject;
        direction.Normalize();
        // set the bullets speed and direction
        bullet.rigidbody2D.velocity = direction * bulletSpeed;
    }

    virtual public float getFireTimer() {
        return fireTimer;
    }

    virtual public void altFire(Vector3 startPosition, Vector2 direction) {

    }

    virtual public float getAltFireTimer() {
        return altFireTimer;
    }

    virtual public void reload() {
        if (bulletsInClip >= clipSize) {
            return;
        }
        Debug.Log("Reloading");
        reloadTimer = reloadDuration;
    }

    virtual public float getReloadTimer() {
        return reloadTimer;
    }

    protected void completeReload() {
        bulletsInClip = clipSize;
        reloadTimer = 0;
    }

    protected void cancelReload() {
        reloadTimer = 0;
    }

    virtual public int getCurrentClip() {
        return bulletsInClip;
    }

    virtual public int getClipSize() {
        return clipSize;
    }
}