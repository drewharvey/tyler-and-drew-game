﻿using UnityEngine;
using System.Collections;

public class CircularExplosion : MonoBehaviour, Explosion, DoesDamage {

    public float secondsToLive;
    public int baseDamage;
    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	void Update () {
        if (startTime + secondsToLive < Time.realtimeSinceStartup) {
            Object.Destroy(this.gameObject);
            Object.Destroy(this);
        }
	}

    #region Explosion Inteface
    public void setRadius(float radius) {
        transform.localScale = new Vector3(radius, radius, transform.localScale.z);
    }
    #endregion

    #region DoesDamage Interface
    public int getDamage() {
        return baseDamage;
    }
    public void setDamage(int dmg) {
        baseDamage = dmg;
    }
    #endregion

}
