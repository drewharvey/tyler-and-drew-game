﻿using UnityEngine;
using System.Collections;

public class BaseBullet : MonoBehaviour, IBullet, DoesDamage {

    public GameObject explosionPrefab;
    public float size;
    public float weight;
    public float gravity;
    public int directHitDamage;
    public int explosionDamage;
    public float explosionRadius;


    // Use this for initialization
    virtual public void Start() {
        rigidbody2D.mass = weight;
        rigidbody2D.gravityScale = gravity;
    }

    // Update is called once per frame
    virtual public void Update() {

    }

    virtual public void onPlayerHit() {
        explode();
        destroy();
    }

    virtual public void explode() {
        // create explosion if we have one
        if (explosionPrefab != null) {
            // create new explosion object
            GameObject explosionObj = (Instantiate(explosionPrefab, rigidbody2D.position, transform.rotation)) as GameObject;
            Explosion explosion = explosionObj.GetComponent(typeof(Explosion)) as Explosion;
            explosion.setRadius(explosionRadius);
            DoesDamage damage = explosionObj.GetComponent(typeof(DoesDamage)) as DoesDamage;
            damage.setDamage(explosionDamage);
        }
    }

    virtual public void destroy() {
        // remove bullet
        Object.Destroy(this.gameObject);
        Object.Destroy(this);
    }

    #region DoesDamage Interface
    virtual public int getDamage() {
        return directHitDamage;
    }
    virtual public void setDamage(int dmg) {
        directHitDamage = dmg;
    }
    #endregion

    #region Collision
    virtual protected void OnCollisionEnter2D(Collision2D other) {
        // let the Player class handle collision for players
        if (other.transform.tag != "Player") {
            explode();
            destroy();
        }
    }
    virtual protected void OnTriggerEnter2D(Collider2D other) {
        // let the Player class handle collision for players
        if (other.transform.tag != "Player") {
            explode();
            destroy();
        }
    }
    #endregion
}
