﻿using UnityEngine;
using System.Collections;

public interface IBullet {
    void onPlayerHit();
}
