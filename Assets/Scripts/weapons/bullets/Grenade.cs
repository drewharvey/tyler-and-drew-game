﻿using UnityEngine;
using System.Collections;

public class Grenade : BaseBullet {

    public float liveDuration;

    private float liveTimer;

    override public void Start() {
        liveTimer = liveDuration;
        base.Start();
    }

    override public void Update() {
        liveTimer -= Time.deltaTime;
        if (liveTimer < 0) {
            this.explode();
            this.destroy();
        }
    }

    override public void onPlayerHit() {
        // dont explode on player hit
    }

    #region Collision
    override protected void OnCollisionEnter2D(Collision2D other) {
        // dont explode on impact
    }
    override protected void OnTriggerEnter2D(Collider2D other) {
        // dont explode on impact
    }
    #endregion

}
