﻿using UnityEngine;
using System.Collections;

public interface IWeapon {
    /// <summary>
    /// Triggers weapons normal attack.
    /// </summary>
    void fire(Vector3 startPosition, Vector2 direction);

    /// <summary>
    /// Triggers weapons secondary attack.
    /// </summary>
    void altFire(Vector3 startPosition, Vector2 direction);

    /// <summary>
    /// Starts this weapons reload sequence.
    /// </summary>
    void reload();

    /// <summary>
    /// Gets time remaining before primary attack is ready to be used. 
    /// </summary>
    float getFireTimer();

    /// <summary>
    /// Gets time remaining before alt attack is ready to be used. 
    /// </summary>
    float getAltFireTimer();

    /// <summary>
    /// Gets time remaining for current reload.  Will return 0 if a reload is complete
    /// or is not in progress.
    /// </summary>
    float getReloadTimer();

    string getWeaponName();
    int getCurrentClip();
    int getClipSize();
}
